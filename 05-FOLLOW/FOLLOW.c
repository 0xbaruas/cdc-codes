/*
 * =====================================================================================
 *
 *       Filename:  FOLLOW.c
 *
 *    Description: C-program to calculate Follow(A) 
 *
 *        Version:  1.0
 *        Created:  09/08/2023 03:01:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 * =====================================================================================
 */

// Lab 5b wap to find follow of given grammar


#include <stdio.h>
#include <string.h>
#include <ctype.h>

int n, p, i = 0, j = 0;
char a[10][10], Result[10];
char subResult[20];
void follow(char *Result, char c);
void first(char *Result, char c);
void addToResultSet(char[], char);

int main() {
    char choice;
    char c, ch;
    printf("Enter the no. of productions: ");
    scanf("%d", &n);

    printf(" Enter %d productions\n Production with multiple terms should be give as separate productions \n", n);
    for (int i = 0; i < n; i++)
        scanf("%s", a[i]);

    do {
        printf("\nFind FOLLOW of : ");
        scanf(" %c", &c);
        follow(Result, c);

        printf("\tFOLLOW(%c) = { ", c);
        for (i = 0; Result[i] != '\0'; i++)
            printf(" %c ", Result[i]);
        printf(" }\n");

        printf("Press 'y' to continue : ");
        scanf(" %c", &choice);
        
    } while (choice == 'y' || choice == 'Y');
}

void follow(char *Result, char c) {
    subResult[0] = '\0';
    Result[0] = '\0';
    if (a[0][0] == c)
        addToResultSet(Result, '$');
    
    for (int i = 0; i < n; i++) {
        for (int j = 2; j < (int)strlen(a[i]); j++) {
            if (a[i][j] == c) {
                if (a[i][j + 1] != '\0')
                    first(subResult, a[i][j + 1]);

                if (a[i][j + 1] == '\0' && c != a[i][0])
                    follow(subResult, a[i][0]);

                for (int k = 0; subResult[k] != '\0'; k++)
                    addToResultSet(Result, subResult[k]);
            }
        }
    }
}

void first(char *R, char c) {
    if (!(isupper(c)) && c != '#')
        addToResultSet(R, c);

    for (int k = 0; k < n; k++) {
        if (a[k][0] == c) {
            if (a[k][2] == '#' && c != a[i][0])
                follow(R, a[i][0]);
            else if ((!(isupper(a[k][2]))) && a[k][2] != '#')
                addToResultSet(R, a[k][2]);
            else
                first(R, a[k][2]);

            for (int m = 0; R[m] != '\0'; m++)
                addToResultSet(Result, R[m]);
        }
    }
}

void addToResultSet(char Result[], char val) {

    int k;
    for (k = 0; Result[k] != '\0'; k++) {
        if (Result[k] == val)
            return;
    }
    Result[k] = val;
    Result[k + 1] = '\0';
}



/*  EXPECTED OUTPUT
    
    For grammar

    S -> AaB
    A -> AbC | D 
    B -> CE
    D -> dD
    E -> eE


    FOLLOW(S) = { $  }
    FOLLOW(A) = { a, b }
    FOLLOW(B) = { $ }
    FOLLOW(C) = { a, b, e }
    FOLLOW(D) = { a, b }
    FOLLOW(E) = { $ }

x=a+b-c/d*2
*/

