/*
 * =====================================================================================
 *
 *       Filename:  valid_identifier_or_not.c
 *
 *    Description: Write a C program to test ~ether a given Identifier Is valid or not 
 *
 *        Version:  1.0
 *        Created:  09/08/2023 12:52:17 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int main() {
    char input[10];
    int flag, i=1;

    printf("Enter an identifier: ");
    scanf("%s",input);

    if(isalpha(input[0]) || input[0]== '_')
        flag = 1;
    else {
        printf("Not a valid identifier\n");
        exit(0);
    }


    while (input[i]!= '\0') {
        if (!isdigit(input[i]) && !isalpha(input[i]) && input[i]!='_') {
            flag = 0;
            break; 
        }
        i++;
    }

    if(flag==1) 
        printf("Valid identifier\n");
    else 
        printf("Not a valid identifier\n");

    return 0;
}

