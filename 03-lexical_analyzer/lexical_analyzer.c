/*
 * =====================================================================================
 *
 *       Filename:  lexical_analyzer.c
 *
 *    Description: write a program for Lexical Analyzer In C 
 *
 *        Version:  1.0
 *        Created:  09/08/2023 12:59:35 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int isKeyword(char buffer[]) {
    char keywords[32][10] = {
        "auto", "break", "case", "char", "const", "continue", "default",
        "do","double","else","enum","extern","float","for","goto",
        "if","int","long","register","return","short","signed",
        "sizeof","static","struct","switch","typedef","union",
        "unsigned","void","volatile","while"
    };

    int flag = 0;
    for (int i = 0; i < 32; ++i) {
        if(strcmp(keywords[i], buffer) == 0)
        {
            flag=1;
            break;
        }
    }
    return flag;
}

int main(int argc, char *argv[])
{
    char ch, buffer[15];
    char operators[] = "+-*/%=";

    FILE *fp;
    int j=0;
    fp = fopen("test.c","r");
    if (fp == NULL) {
        printf("Error while opening the file\n");    
        exit(0);
    }

    while((ch = fgetc(fp)) != EOF){
        for (int i = 0; i < 6; ++i) {
            if(ch == operators[i]) {
                printf("'%c' is operator\n", ch);
            }
        }

        if(isalnum(ch)) {
            buffer[j++] = ch;
        }else if((ch == ' ' || ch == '\n') && (j != 0)) {
            buffer[j] = '\0';
            j = 0;

            if(isKeyword(buffer) == 1)
                printf("'%s' is keyword\n", buffer);
            else 
                printf("'%s' is identifier\n",buffer);
        }
    }
    fclose(fp);
    return 0;
}
