%option noyywrap

%{
    /*
    DEFINITIONS
    */
%}

%{
    int line_num=1;    
%}

digit [0-9]
group {digit}{4}

%%

%{
    /* ***
    RULES
    **** */    
%}

^{group}([ -]?{group}){3}$      { printf("credit card number : %s\n", yytext); }
.*                              { printf("%d: error: %s \n",line_num, yytext); }
\n                              { line_num++; }
%%

int main() {
  yylex();
  return 0;
}