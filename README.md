## Error

![error](./.images/error.png)

## Solution

Step 1: 

![step1](./.images/Tools.jpg)



Step 2: 

![step2](./.images/compiler_options.jpg)



Step 3: 

![step3](./.images/add_options.jpg)



Step 4: Save by clicking "Ok".
