/*
 * =====================================================================================
 *
 *       Filename:  recognize-tokens.c
 *
 *    Description: Write a C program to recognize strings under 'a••, 'a•b+', 'abb' 
 *
 *        Version:  1.0
 *        Created:  09/08/2023 12:08:24 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *       USAGE : gcc recognize-tokens.c -o out -Wall -Wextra
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {

    char s[20], c;
    int state=0, i=0;

    printf("Enter a string: ");
    scanf("%s",s);

    while (s[i]!= '\0') {
        switch (state) {
            case 0:
                c = s[i++];
                if (c=='a') 
                    state=1;
                else if(c=='b')
                    state=2;
                else 
                    state=6;
                break;

            case 1:
                c = s[i++];
                if (c=='a') 
                    state=3;
                else if(c=='b')
                    state=4;
                else 
                    state=6;
                break;

            case 2:
                c = s[i++];
                if (c=='a') 
                    state=6;
                else if(c=='b')
                    state=2;
                else 
                    state=6;
                break;

            case 3:
                c = s[i++];
                if (c=='a') 
                    state=3;
                else if(c=='b')
                    state=2;
                else 
                    state=6;
                break;
            
            case 4:
                c = s[i++];
                if (c=='a') 
                    state=6;
                else if(c=='b')
                    state=5;
                else 
                    state=6;
                break;

            case 5:
                c = s[i++];
                if (c=='a') 
                    state=6;
                else if(c=='b')
                    state=2;
                else 
                    state=6;
                break;

            case 6:
                printf("\n '%s' is not recognized.\n",s);
                exit(0);
        }
    }

    if ((state==3)||(state==1)) {
        printf("\n '%s' is accepted under rule 'a**'\n",s);
    } else if ((state==2) || (state==4)) {
        printf("\n '%s' is accepted under rule 'a*b+'\n",s);
    } else if (state==5) {
        printf("\n '%s' is acceted under rule 'abb'\n",s);
    } else if(state==6) {
        printf("\n '%s' is not recognized.\n",s);
    } 

    return 0;
}

