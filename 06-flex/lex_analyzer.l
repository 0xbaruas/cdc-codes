%{
#include <stdio.h>
#include <stdlib.h>
%}


%option noyywrap  

DIGIT	 	[0-9]
LETTER 		[A-Za-z]
DELIM 		[ \t\n]
WS 			{DELIM}+
ID 			{LETTER}({LETTER}|{DIGIT})*
INTEGER 	{DIGIT}+
%%
{WS}        { /* Do nothing for whitespace */ }
if          { printf("Keyword: \t\t if\n"); }
else        { printf("Keyword: \t\t else\n"); }
{ID}        { printf("Identifier: \t\t %s\n", yytext); }
{INTEGER}   { printf("Integer: \t\t %s\n", yytext); }
"="	        { printf("Assignment Operator:\t %s\n", yytext); }
">"	        { printf("Relational Operator:\t %s\n", yytext); }
"<"         { printf("Relational Operator:\t %s\n", yytext); }
"<="        { printf("Relational Operator:\t %s\n", yytext); }
"=>"        { printf("Relational Operator:\t %s\n", yytext); }
"=="        { printf("Relational Operator:\t %s\n", yytext); }
"!="        { printf("Logical Operator:\t %s\n", yytext); }
"&&"        { printf("Logical Operator:\t %s\n", yytext); }
"||"        { printf("Logical Operator:\t %s\n", yytext); }
"!"         { printf("Logical Operator:\t %s\n", yytext); }
"+"         { printf("Arithmetic Operator:\t %s\n", yytext); }
"-"         { printf("Arithmetic Operator:\t %s\n", yytext); }
"*"	        { printf("Arithmetic Operator:\t %s\n", yytext); }
"/"         { printf("Arithmetic Operator:\t %s\n", yytext); }
"%"         { printf("Arithmetic Operator:\t %s\n", yytext); }
"("         { printf("Open parenthesis:\t %s\n", yytext); }
")"         { printf("Close parenthesis:\t %s\n", yytext); }
"{"         { printf("Open curly braces:\t %s\n", yytext); }
"}"         { printf("Close curly braces:\t %s\n", yytext); }
";"         { printf("Semicolon:\t\t %s\n", yytext); }
"\""        { printf("Double quote :\t\t %s\n", yytext); }
\"([^\"\n]*)\"   { printf("String Constant:\t %s\n", yytext); }
.			{ printf("Unrecognized Token:\t %s\n", yytext); }
%%

int main(int argc, char *argv[])
{
    /* ***
    
    FILE *source;
    if(argc ==2 ) {
        source = fopen(argv[1], "r");
        if(!source) {
            fprintf(stderr, "Error opening souce file : %s\n",argv[1]);
            return 1;
        }
    } else {
        printf("Enter the source code: ");
        source = stdin;
    }
    **** */
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <source_file>\n", argv[0]);
        return 1;
    }

    FILE *source = fopen(argv[1], "r");
    if (!source) {
        perror("Error opening source file");
        return 1;
    }

    yyin = source; // Set Flex to read from the source file



    yyin = source;

    printf("\n================================\n");
    printf("Tokens types\t\t Tokens \n");
    printf("================================\n");
    while(yylex() != 0) {}
    printf("================================\n");

    if(source != stdin)     fclose(source); 
    
    return 0;
}
