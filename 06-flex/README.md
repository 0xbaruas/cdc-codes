## Compiling and Running Flex file

### First open directory of file of `lex_analyzer.l` in `cmd`
    for e.g. 
            if `lex_analyzer.l` is in `Downloads` directory
            then
                > `cd c:\Downloads`  for windows
                > `cd ~/Downloads`   for Linux  


### command to run flex 
```shell
    > flex lex_analyzer.l  					# produces `lex.yy.c` as output
    > gcc lex.yy.c -o lexer                 # (here -lfl flag is optional for windows)
    > ./lexer <input_file>     				# e.g. ./lexer new.cc
```



